import React, {useEffect} from 'react';

import SplashScreen from 'react-native-splash-screen';
import {NativeBaseProvider} from 'native-base/src/core/NativeBaseProvider';
import {extendTheme} from 'native-base';
import {Routes} from '@app/route/Routes';
import {TooltipComponent} from '@app/shared/TooltipComponent';
import {TourGuideProvider} from 'rn-tourguide';

const App = () => {
  const theme = extendTheme({
    components: {},
    config: {
      initialColorMode: 'dark',
    },
  });

  useEffect(() => {
    SplashScreen.hide();
  });

  return (
    // <StylesProvider jss={jss} generateClassName={generateClassName}>
    // <Provider store={store}>
    // <MuiPickersUtilsProvider utils={DateFnsUtils}>
    // <Auth>
    // <Router history={history}>
    // <FuseAuthorization>
    // <FuseTheme>
    // <SnackbarProvider
    //<FuseLayout />
    <NativeBaseProvider theme={theme}>
      <TourGuideProvider
        {...{
          borderRadius: 25,
          backdropColor: 'rgba(50, 50, 50, 0.9)',
          tooltipComponent: TooltipComponent,
          verticalOffset: 25,
        }}>
        <Routes />
      </TourGuideProvider>
    </NativeBaseProvider>
  );
};

export default App;

import {StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import {Text, View} from 'native-base';

export const TooltipComponent = ({
  isFirstStep,
  isLastStep,
  handleNext,
  handlePrev,
  handleStop,
  currentStep,
  labels,
}) => (
  <View
    style={{
      borderRadius: 16,
      paddingTop: 24,
      alignItems: 'center',
      justifyContent: 'center',
      paddingBottom: 16,
      width: '80%',
      backgroundColor: '#595B5D',
    }}>
    <View style={[styles.container]}>
      <Text testID="stepDescription" style={[styles.tooltipText]}>
        {currentStep && currentStep.text}
      </Text>
    </View>
    <View style={[styles.bottomBar]}>
      {!isLastStep ? (
        <TouchableOpacity onPress={handleStop}>
          <Text style={styles.actions}>Skip</Text>
        </TouchableOpacity>
      ) : null}
      {!isFirstStep ? (
        <TouchableOpacity onPress={handlePrev}>
          <Text style={styles.actions}>Previous</Text>
        </TouchableOpacity>
      ) : null}
      {!isLastStep ? (
        <TouchableOpacity onPress={handleNext}>
          <Text style={styles.actions}>Next</Text>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity onPress={handleStop}>
          <Text style={styles.actions}>Finish</Text>
        </TouchableOpacity>
      )}
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '80%',
  },
  tooltipText: {
    textAlign: 'center',
  },
  bottomBar: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  actions: {
    padding: 10,
    color: '#1FA8E0',
  },
});

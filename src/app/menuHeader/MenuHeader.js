import React from 'react';
import {Divider, HamburgerIcon, Menu, QuestionOutlineIcon} from 'native-base';
import {StyleSheet, TouchableOpacity} from 'react-native';

const MenuHeader = ({nav}) => {
  return (
    <Menu
      style={styles.menu}
      trigger={triggerProps => {
        return (
          <TouchableOpacity {...triggerProps}>
            <HamburgerIcon />
          </TouchableOpacity>
        );
      }}>
      <Menu.Item onPress={() => nav.navigate('Guide')}>Guide</Menu.Item>
      <Menu.Item onPress={() => nav.navigate('Setting')}>Setting</Menu.Item>
      <Divider />
      <Menu.Item onPress={() => nav.navigate('About')}>
        <QuestionOutlineIcon />
      </Menu.Item>
    </Menu>
  );
};
const styles = StyleSheet.create({
  menu: {
    width: 100,
    right: 0,
    paddingTop: 0,
    paddingBottom: 0,
    zIndex: 99999,
  },
});

export default MenuHeader;

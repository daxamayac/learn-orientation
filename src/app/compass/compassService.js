class CompassService {
  HEADING_RANGE = 5;

  isCorrectHeading(heading: number, target: number) {
    if (heading === target) {
      return true;
    }

    if (heading >= 360 - this.HEADING_RANGE && target <= this.HEADING_RANGE) {
      heading -= 360;
    } else if (
      target >= 360 - this.HEADING_RANGE &&
      heading <= this.HEADING_RANGE
    ) {
      heading += 360;
    }

    if (heading > target) {
      return this.innerRight(heading, target);
    }
    if (heading < target) {
      return this.innerLeft(heading, target);
    }
  }

  innerRight(heading, target) {
    return heading <= target + this.HEADING_RANGE;
  }

  innerLeft(heading, target) {
    return target - this.HEADING_RANGE <= heading;
  }
}

const instance = new CompassService();

export default instance;

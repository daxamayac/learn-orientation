import React, {useEffect, useState} from 'react';
import {Dimensions, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import CompassHeading from 'react-native-compass-heading';

import Needle from 'assets/compass/needle.svg';
import Dial from 'assets/compass/dial.svg';
import Base from 'assets/compass/base.svg';
import BodyWhite from 'assets/compass/body_white.svg';
import Slider from '@react-native-community/slider';
import compassService from '@app/compass/compassService';

import {TourGuideZone} from 'rn-tourguide';
import {Text, View} from 'native-base';

const DOUBLE_PRESS_DELAY = 300;

const Compass = props => {
  const {ratio, showHelp} = props;
  const [compassHeading, setCompassHeading] = useState(0);
  const [target, setTarget] = useState(0);
  const [color, setColor] = useState('green');
  const [lockedHeading, setBlockHeading] = useState(false);
  const {width, height} = Dimensions.get('window');

  const body = (width < height ? width : height) * ratio;
  const toggleLockedHeading = () =>
    setBlockHeading(previousState => !previousState);

  const size = body - 90 * ratio;

  let lastPress = 0;

  useEffect(() => {
    const degree_update_rate = 1;
    CompassHeading.start(degree_update_rate, ({heading, accuracy}) => {
      //      let randomInt = Math.floor(Math.random() * 30) - 15;
      setCompassHeading(heading);
    });

    return () => {
      CompassHeading.stop();
    };
  }, []);

  useEffect(() => {
    if (compassService.isCorrectHeading(compassHeading, parseInt(target))) {
      setColor('green');
    } else {
      setColor('red');
    }
  }, [compassHeading, target]);

  const handleBlockHeading = () => {
    const now = new Date().getTime();
    if (lastPress && now - lastPress < DOUBLE_PRESS_DELAY) {
      lastPress = 0;
      toggleLockedHeading();
    } else {
      lastPress = now;
    }
  };

  return (
    <>
      <TourGuideZone
        zone={1}
        shape="rectangle"
        text={'Dirección a la que apunta la aguja'}>
        <Text style={{fontSize: 50}}>{compassHeading + '°'}</Text>
      </TourGuideZone>
      {/*{left: -25 * ratio}*/}
      <TourGuideZone
        zone={4}
        shape="circle"
        text={'El perfil se pone en verde si tu curso es correcto'}>
        <View width={body} height={body} style={[styles.container]}>
          <BodyWhite width={body} height={body} style={[styles.body]} />
          <Base
            width={size + 15 * ratio}
            height={size + 15 * ratio}
            style={[
              styles.base,
              {transform: [{rotate: `${360 - target}deg`}]},
              showHelp ? {borderWidth: 10 * ratio} : null,
              color === 'red'
                ? {borderColor: 'red'}
                : {borderColor: 'green', borderStyle: 'solid'},
            ]}
          />
          <TourGuideZone
            zone={3}
            shape="circle"
            text={
              'Doble touch para bloquear el rumbo y el perfil se pone de color verde'
            }
            style={[styles.zone, {width: size - 80}]}>
            <Needle
              width={size}
              height={size}
              style={[
                styles.needle,
                showHelp ? {borderWidth: 32 * ratio} : null,
                {transform: [{rotate: `${360 - compassHeading}deg`}]},
                lockedHeading
                  ? {
                      borderColor: 'green',
                      borderStyle: 'solid',
                      borderWidth: 32 * ratio - 3,
                    }
                  : {borderColor: 'yellow'},
              ]}
            />
          </TourGuideZone>

          <Dial
            width={size}
            height={size}
            style={[styles.dial, {transform: [{rotate: `${360 - target}deg`}]}]}
          />
        </View>
      </TourGuideZone>
      <TouchableWithoutFeedback onPress={() => handleBlockHeading()}>
        <View width={size - 25} height={size - 25} style={[styles.circle]} />
      </TouchableWithoutFeedback>
      <TourGuideZone
        zone={2}
        shape="rectangle"
        text={'Mueve el limbo de la brújala'}>
        <View>
          <Text>Rumbo</Text>
          <Slider
            style={{width: 200, height: 40}}
            minimumValue={0}
            maximumValue={359}
            step={1}
            disabled={lockedHeading}
            minimumTrackTintColor="#FFFFFF"
            maximumTrackTintColor="#000000"
            value={target}
            onValueChange={setTarget}
          />
        </View>
      </TourGuideZone>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'solid',
    borderRightWidth: 2,
    borderLeftWidth: 2,
    borderColor: 'white',
  },
  base: {
    position: 'absolute',
    borderRadius: 500,
    borderStyle: 'dashed',
  },
  needle: {
    position: 'absolute',
    borderRadius: 500,
    borderStyle: 'dashed',
  },
  dial: {
    position: 'absolute',
  },
  body: {
    position: 'relative',
  },
  circle: {
    position: 'absolute',
    borderRadius: 500,
  },
  zone: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    padding: 50,
  },
});
export default Compass;

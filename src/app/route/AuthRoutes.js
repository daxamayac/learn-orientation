import React from 'react';

import CompassScreen from '@app/screen/compass/CompassScreen';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SettingScreen from '@app/screen/settings/SettingScreen';
import AboutScreen from '@app/screen/about/AboutScreen';

import GuideScreen from '@app/screen/guide/GuideScreen';

const {Navigator, Screen} = createNativeStackNavigator();

export function AuthRoutes() {
  return (
    <Navigator>
      <Screen name="Learn Orientation" component={CompassScreen} />
      <Screen name="Guide" component={GuideScreen} />
      <Screen name="Setting" component={SettingScreen} />
      <Screen name="About" component={AboutScreen} />
    </Navigator>
  );
}

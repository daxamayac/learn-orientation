import React from 'react';

import {
  DarkTheme,
  DefaultTheme,
  NavigationContainer,
} from '@react-navigation/native';
import {AuthRoutes} from '@app/route/AuthRoutes';
import {useColorMode} from 'native-base';

export function Routes() {
  const {colorMode} = useColorMode();
  return (
    <NavigationContainer
      theme={colorMode === 'dark' ? DarkTheme : DefaultTheme}>
      <AuthRoutes />
    </NavigationContainer>
  );
}

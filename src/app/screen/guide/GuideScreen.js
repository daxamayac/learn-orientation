import React, {useState} from 'react';
import {View} from 'native-base';
import {SceneMap, TabView} from 'react-native-tab-view';
import {useWindowDimensions} from 'react-native';

const FirstRoute = () => <View style={{flex: 1, backgroundColor: '#ff4081'}} />;

const SecondRoute = () => (
  <View style={{flex: 1, backgroundColor: '#673ab7'}} />
);

const GuideScreen = () => {
  const layout = useWindowDimensions();

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'first', title: 'Mapa'},
    {key: 'second', title: 'Brújulas'},
    {key: 'three', title: 'Técnica'},
    {key: 'four', title: 'Reglas'},
  ]);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    three: FirstRoute,
    four: SecondRoute,
  });

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{width: layout.width}}
    />
  );
};

export default GuideScreen;

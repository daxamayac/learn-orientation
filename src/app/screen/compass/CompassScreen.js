import React, {useEffect, useLayoutEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Slider from '@react-native-community/slider';

import {useTourGuideController} from 'rn-tourguide';
import Compass from '@app/compass/Compass';
import SplashScreen from 'react-native-splash-screen';
import {Center, QuestionOutlineIcon, Switch, Text, View} from 'native-base';
import MenuHeader from '@app/menuHeader/MenuHeader';

const CompassScreen = ({navigation}) => {
  const [ratio, onChangeRatio] = useState(0.9);
  const [showHelp, setShowHelp] = useState(true);
  const {canStart, start} = useTourGuideController();

  useEffect(() => {
    if (canStart) {
      // 👈 test if you can start otherwise nothing will happen
      //  start();
    }
  }, [canStart]);

  const toggleShowHelp = () => setShowHelp(previousState => !previousState);
  useEffect(() => {
    SplashScreen.hide();
  });

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <>
          <TouchableOpacity style={styles.button} onPress={() => start()}>
            <QuestionOutlineIcon />
          </TouchableOpacity>
          <Text> </Text>
          <MenuHeader nav={navigation} />
        </>
      ),
    });
  });

  return (
    <Center flex={1}>
      <View style={[styles.container]}>
        <Compass ratio={ratio} showHelp={showHelp} />
      </View>
      <View style={[styles.container]}>
        <Text>Tamaño</Text>
        <Slider
          style={styles.slider}
          minimumValue={0.25}
          maximumValue={1}
          step={0.2}
          minimumTrackTintColor="#FFFFFF"
          maximumTrackTintColor="#000000"
          value={ratio}
          onValueChange={onChangeRatio}
        />
        <Text>Show help proper heading</Text>
        <Switch
          trackColor={{false: '#767577', true: '#59768D'}}
          thumbColor={showHelp ? '#90CAF9' : '#f4f3f4'}
          ios_backgroundColor="#3e3e3e"
          onToggle={toggleShowHelp}
          isChecked={showHelp}
        />
      </View>
    </Center>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
  },
  slider: {width: 200, height: 40},
});

export default CompassScreen;

/**
 * @format
 */
import compassService from '@app/compass/compassService';

const HEADING_RANGE = compassService.HEADING_RANGE;

describe('test normal cases ', () => {
  it('equals ', () => {
    let value = compassService.isCorrectHeading(50, 50);
    expect(value).toBeTruthy();
  });

  it('limit right ', () => {
    let value = compassService.isCorrectHeading(50 + HEADING_RANGE, 50);
    expect(value).toBeTruthy();
  });

  it('out right ', () => {
    let value = compassService.isCorrectHeading(50 + HEADING_RANGE + 1, 50);
    expect(value).toBeFalsy();
  });

  it('limit left', () => {
    let value = compassService.isCorrectHeading(50 - HEADING_RANGE, 50);
    expect(value).toBeTruthy();
  });

  it('out left ', () => {
    let value = compassService.isCorrectHeading(50 - HEADING_RANGE - 1, 50);
    expect(value).toBeFalsy();
  });
});

describe('test specials case ', () => {
  it('t>=0 in limit left ', () => {
    let value = compassService.isCorrectHeading(355, 0);
    expect(value).toBeTruthy();
  });

  it('t>=0 in limit left 359 ', () => {
    let value = compassService.isCorrectHeading(359, 4);
    expect(value).toBeTruthy();
  });

  it('t>=0 in limit left 0 ', () => {
    let value = compassService.isCorrectHeading(0, 5);
    expect(value).toBeTruthy();
  });

  it('t>=0 out limit left ', () => {
    let value = compassService.isCorrectHeading(355, 10);
    expect(value).toBeFalsy();
  });

  it('t>=0 out limit left and t>5 ', () => {
    let value = compassService.isCorrectHeading(356, 4);
    expect(value).toBeFalsy();
  });

  it('y<=359 in limit left ', () => {
    let value = compassService.isCorrectHeading(355, 359);
    expect(value).toBeTruthy();
  });

  it('y<=359 out in limit left ', () => {
    let value = compassService.isCorrectHeading(353, 359);
    expect(value).toBeFalsy();
  });

  it('y<=359 out in limit left y 0.. ', () => {
    let value = compassService.isCorrectHeading(0, 359);
    expect(value).toBeTruthy();
  });
});

describe('final ', () => {
  it('1', () => {
    let value = compassService.isCorrectHeading(342, 342);
    expect(value).toEqual(true);
  });

  it('2', () => {
    let value = compassService.isCorrectHeading(350, 355);
    expect(value).toEqual(true);
  });
  it('3', () => {
    let value = compassService.isCorrectHeading(350, 356);
    expect(value).toEqual(false);
  });
});
